import {HeroCollection} from "../../../both/collections/hero.collection";
import {Hero} from "../../../both/models/hero.model";

export class Main {
    start(): void {
        this.initDummyData();
    }

    initDummyData(): void {
        if (HeroCollection.find({}).cursor.count() === 0) {
            const data: Hero[] = [{
                name: "Batman",
                age: 26,
                description: "Batman is the superhero protector of Gotham City, a man dressed like a bat who fights against evil and strikes terror into the hearts of criminals everywhere",
                sidekicks: [
                    {
                        name: "Robin",
                        age: 16,
                        description: "Robin has long been a fixture in the Batman comic books as Batman's partner"
                    },
                    {
                        name: "Batgirl",
                        age: 18,
                        description: "An exceptional fighter and detective, Batgirl is also unparalleled in her computer skills."
                    }
                ]
            }, {
                name: "Power Girl",
                age: 22,
                description: "Power Girl is the Earth-Two counterpart of the Kryptonian Supergirl and first cousin to Kal-L, the Superman of Earth-Two",
                sidekicks: [
                    {
                        name: "Terra",
                        age: 16,
                        description: "Terra was a former member of the Teen Titans, whose role as a hero or villain is complicated."
                    }
                ]
            }, {
                name: "Green Arrow",
                age: 32,
                description: "Green Arrow is a vigilante superhero who fights crime using archery, martial arts and technology",
                sidekicks: [
                    {
                        name: "Speedy",
                        age: 26,
                        description: 'A former prostitute on the streets of Star City, Mia Dearden picked up the bow and took the name "Speedy", crime-fighting student of Green Arrow'
                    }
                ]
            }];
            data.forEach((obj: Hero) => {
                HeroCollection.insert(obj);
            });
        }
    }
}
