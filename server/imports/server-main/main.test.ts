// chai uses as asset library
import * as chai from "chai";
import * as spies from "chai-spies";
import StubCollections from "meteor/hwillson:stub-collections";

import { Main } from "./main";
import { HeroCollection } from "../../../both/collections/hero.collection";

chai.use(spies);

describe("Server Main", () => {
  let mainInstance: Main;

  beforeEach(() => {
    // Creating database mock
    StubCollections.stub(HeroCollection);

    // Create instance of main class
    mainInstance = new Main();
  });

  afterEach(() => {
    // Restore database
    StubCollections.restore();
  });

  it("Should call initDummyData on startup", () => {
    mainInstance.initDummyData = chai.spy();
    mainInstance.start();

    chai.expect(mainInstance.initDummyData).to.have.been.called();
  });

  it("Should call insert 3 times when initDummyData", () => {
    HeroCollection.insert = chai.spy();
    mainInstance.initDummyData();

    chai.expect(HeroCollection.insert).to.have.been.called.exactly(3);
  });
});
