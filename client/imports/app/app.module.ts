import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {router} from './app.router';
import {AppComponent} from "./app.component";
import {HeroListComponent} from "./components/hero-list/hero-list.component";
import {HeroService} from "./services/hero/hero.service";
import {HeroProfileComponent} from "./components/hero-profile/hero-profile.component";
import {HeroCreateComponent} from "./components/hero-create/hero-create.component";
import {FormsModule} from "@angular/forms";
import {SidekickService} from "./services/sidekick/sidekick.service";

@NgModule({
    // Components, Pipes, Directive
    declarations: [
        AppComponent,
        HeroListComponent,
        HeroProfileComponent,
        HeroCreateComponent
    ],
    // Entry Components
    entryComponents: [
        AppComponent
    ],
    // Providers
    providers: [
        HeroService,
        SidekickService
    ],
    // Modules
    imports: [
        BrowserModule,
        FormsModule,
        router
    ],
    // Main Component
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor() {

    }
}
