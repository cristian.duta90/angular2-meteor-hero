import {Injectable} from "@angular/core";
import {ObservableCursor} from "meteor-rxjs";
import {Hero} from "../../../../../both/models/hero.model";
import {HeroCollection} from "../../../../../both/collections/hero.collection";
import {Observable} from "rxjs/Observable";

@Injectable()
export class HeroService {

    public getHeroList(): ObservableCursor<Hero> {
        return HeroCollection.find({});
    }

    public getHero(heroName: string): Hero {
        return HeroCollection.findOne({name: heroName});
    }

    public addNewHero(hero: Hero): Observable<string> {
        return HeroCollection.insert(hero);
    }

    public removeHero(hero: Hero): Observable<number> {
        return HeroCollection.remove({_id: hero['_id']});
    }
}
