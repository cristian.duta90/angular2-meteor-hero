import {Injectable} from "@angular/core";
import {HeroCollection} from "../../../../../both/collections/hero.collection";
import {Observable} from "rxjs/Observable";
import {Sidekick} from "../../../../../both/models/sidekick.model";
import {HeroService} from "../hero/hero.service";

@Injectable()
export class SidekickService {

    public constructor(private heroService: HeroService) {

    }

    public getSidekick(heroName: string, sidekickName: string): Sidekick {
        let hero = HeroCollection.findOne(
            {
                name: heroName,
                "sidekicks.name": sidekickName
            }
        );
        if (!hero) {
            return hero;
        }

        let sidekick = hero.sidekicks.filter(sidekick => sidekick.name == sidekickName);
        return sidekick[0];
    }

    public addNewSidekick(heroName: string, sidekick: Sidekick): Observable<number> {
        return HeroCollection.update(
            {
                _id: this.heroService.getHero(heroName)['_id'] /* can only be done by _id since it's untrusted code */
            }, {
                $push: {
                    "sidekicks": sidekick
                }
            }
        );
    }

    public removeSidekick(heroName: string, sidekick: Sidekick): Observable<number> {
        return HeroCollection.update(
            {
                _id: this.heroService.getHero(heroName)['_id'] /* can only be done by _id since it's untrusted code */
            }, {
                $pull: {
                    "sidekicks": sidekick
                }
            }
        );
    }
}
