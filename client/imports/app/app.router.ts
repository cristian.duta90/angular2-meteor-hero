import {RouterModule, Routes} from "@angular/router";
import {HeroListComponent} from "./components/hero-list/hero-list.component";
import {HeroProfileComponent} from "./components/hero-profile/hero-profile.component";
import {HeroCreateComponent} from "./components/hero-create/hero-create.component";

let appRoutes: Routes = [
    {
        path: 'heroes',
        component: HeroListComponent
    },
    {
        path: 'hero/create',
        component: HeroCreateComponent
    },
    {
        path: 'hero/:heroName/sidekick/create',
        component: HeroCreateComponent
    },
    {
        path: 'hero/:heroName',
        component: HeroProfileComponent
    },
    {
        path: 'hero/:heroName/sidekick/:sidekickName',
        component: HeroProfileComponent
    },
    {
        path: '',
        component: HeroListComponent
    },
    {
        path: '**',
        component: HeroListComponent
    }
];

export const router = RouterModule.forRoot(appRoutes);
