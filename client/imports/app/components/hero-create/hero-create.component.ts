import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import template from "./hero-create.component.html";
import style from "./hero-create.component.scss";
import {HeroService} from "../../services/hero/hero.service";
import {Hero} from "../../../../../both/models/hero.model";
import {Sidekick} from "../../../../../both/models/sidekick.model";
import {SidekickService} from "../../services/sidekick/sidekick.service";
import {Location} from "@angular/common";

@Component({
    selector: "hero-create",
    template,
    styles: [style]
})
export class HeroCreateComponent implements OnInit {
    public hero: Hero | Sidekick;
    public heroName: string; // gets a value if the current hero is a Sidekick
    public error: string;

    constructor(private heroService: HeroService,
                private sidekickService: SidekickService,
                private route: ActivatedRoute,
                private _location: Location) {
    }

    /**
     * Check the route if the current hero is a Sidekick and then set the appropriate used values
     */
    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params['heroName']) {
                this.hero = {
                    name: "",
                    age: 1,
                    description: ""
                };

                this.heroName = params['heroName'];
            } else {
                this.hero = {
                    name: "",
                    age: 1,
                    description: "",
                    sidekicks: []
                };
            }
        });
    }

    /**
     * Handle hero creation
     */
    public createHero() {
        if (this.isSidekick()) {
            if (this.isValidSidekick(this.hero as Sidekick)) {
                this.sidekickService.addNewSidekick(this.heroName, this.hero as Sidekick);
            }
        } else {
            if (this.isValidHero(this.hero as Hero)) {
                this.heroService.addNewHero(this.hero as Hero);
            }
        }
        if (!this.error) {
            this._location.back();
        }
    }

    /**
     * Check if a hero with the same name does not exist already
     * @param hero
     * @returns {boolean}
     */
    private isValidHero(hero: Hero) {
        if (this.heroService.getHero(hero.name) !== undefined) {
            this.error = "Hero already exists!";
            return false;
        }
        this.error = "";
        return true;
    }

    /**
     * Check if a sidekick with the same name for this hero does not exist already
     * @param sidekick
     * @returns {boolean}
     */
    private isValidSidekick(sidekick: Sidekick) {
        if (this.sidekickService.getSidekick(this.heroName, sidekick.name) !== undefined) {
            this.error = "Sidekick already exists!";
            return false;
        }
        this.error = "";
        return true;
    }

    /**
     * Check if current hero is a Sidekick or a Hero
     * @returns {boolean}
     */
    private isSidekick() {
        return !!this.heroName;
    }
}
