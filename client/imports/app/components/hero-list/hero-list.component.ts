import {Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import template from "./hero-list.component.html";
import style from "./hero-list.component.scss";
import {HeroService} from "../../services/hero/hero.service";
import {Hero} from "../../../../../both/models/hero.model";

@Component({
    selector: "hero-list",
    template,
    styles: [style]
})
export class HeroListComponent implements OnInit {
    heroList: Observable<Hero[]>;

    constructor(private heroService: HeroService) {
    }

    ngOnInit() {
        this.heroList = this.heroService.getHeroList().zone();
    }
}
