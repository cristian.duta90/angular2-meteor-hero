import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import template from "./hero-profile.component.html";
import style from "./hero-profile.component.scss";
import {HeroService} from "../../services/hero/hero.service";
import {Hero} from "../../../../../both/models/hero.model";
import {Sidekick} from "../../../../../both/models/sidekick.model";
import {SidekickService} from "../../services/sidekick/sidekick.service";
import {Location} from "@angular/common";

@Component({
    selector: "hero-profile",
    template,
    styles: [style]
})
export class HeroProfileComponent implements OnInit {
    public hero: Hero | Sidekick;
    private _heroName: string;

    constructor(private heroService: HeroService,
                private sidekickService: SidekickService,
                private route: ActivatedRoute,
                private router: Router,
                private _location: Location) {
    }

    /**
     * Retrieve the data from the db depending on whatever the current hero is a Sidekick or Hero
     */
    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params['heroName'] && params['sidekickName']) {
                this.hero = this.sidekickService.getSidekick(params['heroName'], params['sidekickName']);
                this._heroName = params['heroName'];
            } else if (params['heroName']) {
                this.hero = this.heroService.getHero(params['heroName']);
            }
        });
    }

    /**
     * Remove the current Hero/Sidekick
     */
    public removeHero() {
        if (this._heroName) {
            this.sidekickService.removeSidekick(this._heroName, this.hero as Sidekick);
            this._location.back();
        } else {
            this.heroService.removeHero(this.hero as Hero);
        }
    }
}
