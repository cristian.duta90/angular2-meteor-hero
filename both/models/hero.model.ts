import {Sidekick} from "./sidekick.model";

export interface Hero {
    name: string;
    age: number;
    description: string;
    sidekicks: Sidekick[]
}
