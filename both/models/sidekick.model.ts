export interface Sidekick {
    name: string;
    age: number;
    description: string;
}
